import webpack from 'webpack';
import BowerWebpackPlugin from 'bower-webpack-plugin';
import path  from 'path';


export default {
  debug: true,
  devtool: 'cheap-module-eval-source-map',
  noInfo: false, // display list of packages that are being bundled
  entry: [
    'webpack-hot-middleware/client?reload=true', // reloads the page if hot module
    path.join(__dirname, 'app')
  ],
  // tell webpack how to bundle the code
  // web tells webpack that this code will run in
  // a web browser.
  target: 'web',
  // physical files are only output for production builds
  // during development output files will be saved in MEMORY.
  output:{
    path: path.join(__dirname, 'public', 'js'),
    publicPath: '/',
    filename: 'client.bundle.js'
  },
  devServer: {
    contentBase: path.join(__dirname, 'lib', 'index.ejs')
  },
  plugins: [
    new BowerWebpackPlugin(),
    //new ExtractTextPlugin('client.bundle.css'), // name of compiled css file
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(), // keep errors from breaking reload
    new webpack.ProvidePlugin({
      $:      'jquery',
      jQuery: 'jquery'
    })
  ],
  // types of files to handle
  module: {
    loaders: [
      { test: /\.js$/, include: path.join(__dirname, 'lib'), loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.scss$/, loader: 'style!css?sourceMap!sass?sourceMap&sourceComments', exclude: /node_modules/ },
      { test: /\.(woff|svg|ttf|eot)([\?]?.*)$/, loader: 'file-loader?name=[name].[ext]', exclude: /node_modules/ }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.scss']
  }
};
