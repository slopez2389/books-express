import express from 'express';
import webpack from 'webpack';
import open from 'open';
import config from '../webpack.config.dev';
import settings from '../lib/server/common/settings';
import routes from '../lib/server/routes';


/*eslint-disable no-console*/

const port = 3000;
const app = express();
const compiler = webpack(config);

// app.use allows us to set up middleware
// it will be used by express first before
// anything else is done.
app.use(express.static('public'));

app.use(express.static('lib/client'));

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.use('/books', routes.bookRouter);
app.use('/authors', routes.authorRouter);

app.set('views', ['lib/client', 'lib/client/views']);
app.set('view engine', 'ejs');



app.get('/', function (req, res) {
  res.render('index',
    {
      title: 'Homepage',
      nav: settings.nav
    });
});


app.listen(port, function(err){
  if(err){
    console.log(err);
  } else {
    open(`http://localhost:${port}`);
  }
});
