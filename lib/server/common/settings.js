export default {
  title: 'Express Library',
  nav: [
    { link: '/books', label: 'Books' },
    { link: '/authors', label: 'Authors' }
  ]
};
