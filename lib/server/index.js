'use strict';
/* eslint-disable no-console */
import express from 'express';

const app = express();
const port = process.env.PORT || 5000;
import routes from './routes';
import settings from './common/settings';

// app.use allows us to set up middleware
// it will be used by express first before
// anything else is done.
app.use(express.static('public'));
app.use(express.static('lib/client'));

app.set('views', ['lib/client', 'lib/client/views']);
app.set('view engine', 'ejs');

app.get('/', function (req, res) {
  res.render('index',
    {
      title: 'Homepage',
      nav: settings.nav
    });
});

app.use('/books', routes.bookRouter);
app.use('/authors', routes.authorRouter);

app.listen(port, function(err){
  console.log('Running server on ', port);
});

module.exports = app;