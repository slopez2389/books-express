import bookRouter from'./book';
import authorRouter from'./author';

export default {
  bookRouter,
  authorRouter
};